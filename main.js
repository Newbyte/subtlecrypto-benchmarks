const rsa_keygen_form = document.getElementById("rsa-keygen-form");
const rsa_derive_form = document.getElementById("rsa-derive-form");
const ec_keygen_form = document.getElementById("ec-keygen-form");
const progress_container = document.getElementById("progress-container");
const reset_log_button = document.getElementById("reset-log");
const benchmark_forms = document.getElementsByClassName("benchmark-form");
const benchmark_selectors =
	document.getElementsByClassName("benchmark-selector");
const encoder = new TextEncoder();

update_visible_benchmark();

document.addEventListener("visibilitychange", function() {
	if (!document.hidden) {
		reset_title();
	}
});

rsa_keygen_form.addEventListener("submit", function(evnt) {
	evnt.preventDefault();
	
	const modulus_length = parseInt(evnt.target[0].value);
	const increment = parseInt(evnt.target[1].value);
	const modulus_value_limit = parseInt(evnt.target[2].value);
	const max_iterations = parseInt(evnt.target[3].value);
	
	gen_key(modulus_length, increment, modulus_value_limit, max_iterations);
});

rsa_derive_form.addEventListener("submit", async function(evnt) {
	evnt.preventDefault();

	const derive_material = evnt.target[0].value;
	const derive_iterations = parseInt(evnt.target[1].value);

	const key_to_derive = await crypto.subtle.importKey(
		"raw",
		encoder.encode(derive_material),
		{
			"name": "PBKDF2"
		},
		false,
		["deriveBits", "deriveKey"]
	);

	const salt = crypto.getRandomValues(new Uint8Array(16));

	derive_key(key_to_derive, derive_iterations, salt);
});

ec_keygen_form.addEventListener("submit", function(evnt) {
	evnt.preventDefault();

	const algorithm_name = evnt.target[0].value;
	const curve_name = evnt.target[1].value;
	const number_iterations = evnt.target[2].value;

	gen_ec_key(algorithm_name, curve_name, number_iterations);
});

reset_log_button.addEventListener("click", function() {
	while (progress_container.lastChild) {
		progress_container.lastChild.remove();
	}
});

for (let i = 0; i < benchmark_selectors.length; i++) {
	benchmark_selectors[i].addEventListener("change", function() {
		update_visible_benchmark();
	});
}

function gen_rsa_key(
	modulus_length,
	increment,
	limit,
	max_iter = Infinity,
	iter = 1,
	times = []
) {
	const gen_message = "Generating RSA keypair, modulus length " + modulus_length.toString() + " ...";
	const status_text = document.createElement("P");
	status_text.textContent = gen_message;
	console.log(gen_message);
	new_message(status_text);
	document.title = "Running benchmark ...";

	const start_time = performance.now();
	crypto.subtle.generateKey({
			name: "RSA-OAEP",
			modulusLength: modulus_length,
			publicExponent: new Uint8Array([1, 0, 1]),
			hash: "SHA-512"
		},
		true,
		["encrypt", "decrypt"]
	).then(function(keypair) {
		const delta = performance.now() - start_time;
		console.log("Keypair generated!");
		console.log(keypair);
		status_text.textContent += " OK! (" + delta.toString() + " ms)";
		times.push(delta);
		
		if (modulus_length < limit && iter < max_iter) {
			iter++;
			gen_rsa_key(modulus_length + increment, increment, limit, max_iter, iter, times);
		} else {
			if (document.hidden) {
				document.title = "Benchmark done!";
			} else {
				reset_title();
			}
			const max_time = Math.max(...times).toString();
			const min_time = Math.min(...times).toString();
			const avg_time = 
				(times.reduce((prev, curr) => prev + curr) / times.length)
				.toString();
			
			const done_message =
				"Done! Max time: " + max_time +
				" ms, min time: " + min_time +
				" ms, avg_time: " + avg_time + " ms";
			const done_text = document.createElement("P");
			done_text.textContent = done_message;
			console.log(done_message);
			new_message(done_text);
		}
	})
}

async function derive_key(key_material, derive_iterations, salt) {
	const message_element = document.createElement("P");

	message_element.textContent
		= "Deriving key, iterations " + derive_iterations + " ...";

	new_message(message_element);

	const start_time = performance.now();

	console.log(key_material);

	await crypto.subtle.deriveKey(
		{
			"name": "PBKDF2",
			"salt": salt,
			"iterations": derive_iterations,
			"hash": "SHA-512"
		},
		key_material,
		{
			"name": "AES-GCM",
			"length": 256
		},
		false,
		[]
	);

	const delta = performance.now() - start_time;

	message_element.textContent += " OK! (" + delta.toString() + " ms)";
}

function gen_ec_key(
	algorithm_name,
	curve_name,
	max_iter = Infinity,
	iter = 1,
	times = []
) {
	const gen_message = "Generating " + algorithm_name + " keypair, curve " + curve_name + " ...";
	const status_text = document.createElement("P");
	status_text.textContent = gen_message;
	console.log(gen_message);
	new_message(status_text);
	document.title = "Running benchmark ...";

	let usage;

	if (algorithm_name === "ECDSA") {
		usage = ["sign", "verify"];
	} else {
		usage = ["deriveKey"];
	}

	const start_time = performance.now();
	crypto.subtle.generateKey(
		{
			name: algorithm_name,
			namedCurve: curve_name
		},
		true,
		usage
	).then(function(keypair) {
		const delta = performance.now() - start_time;
		console.log("Keypair generated!");
		console.log(keypair);
		status_text.textContent += " OK! (" + delta.toString() + " ms)";
		times.push(delta);

		if (iter < max_iter) {
			iter++;
			gen_ec_key(algorithm_name, curve_name, max_iter, iter, times);
		} else {
			if (document.hidden) {
				document.title = "Benchmark done!";
			} else {
				reset_title();
			}
			const max_time = Math.max(...times).toString();
			const min_time = Math.min(...times).toString();
			const avg_time =
				(times.reduce((prev, curr) => prev + curr) / times.length)
				.toString();

			const done_message =
				"Done! Max time: " + max_time +
				" ms, min time: " + min_time +
				" ms, avg_time: " + avg_time + " ms";
			const done_text = document.createElement("P");
			done_text.textContent = done_message;
			console.log(done_message);
			new_message(done_text);
		}
	})
}

function reset_title() {
	document.title = "SubtleCrypto Benchmarks";
}

function new_message(message) {
	progress_container.appendChild(message);
}

function update_visible_benchmark() {
	for (let i = 0; i < benchmark_forms.length; i++) {
		if (benchmark_selectors[i].checked) {
			benchmark_forms[i].hidden = false;
		} else {
			benchmark_forms[i].hidden = true;
		}
	}

}

