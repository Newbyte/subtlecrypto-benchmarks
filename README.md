# SubtleCrypto Benchmarks

Quick-and-dirty benchmarks for evaluating performance of the SubtleCrypto API. 

Hosted on GitLab pages: https://newbyte.gitlab.io/subtlecrypto-benchmarks/
